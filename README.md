# Planeta

Le sous-repertoire planeta est la derniere version dev disponible sur https://framagit.org/calandreta/planeta


## Installer docker 

Installation Linux: https://docs.docker.com/desktop/install/linux-install/#generic-installation-steps
Installation Windows: https://docs.docker.com/desktop/install/windows-install/#install-docker-desktop-on-windows

## Installer docker-compose

Installation Linux: https://docs.docker.com/compose/install/linux/
Installation Windows (idem docker): https://docs.docker.com/desktop/install/windows-install/#install-docker-desktop-on-windows

## Installation de planeta

#### Cloner le repository localement puis se placer dans le répertoire copié: 
`git clone https://gitlab.com/aurelpere/planeta.git`

#### Lancer phpmyadmin et mysql
`cd planeta`
`docker-compose up`
Note : vous pouvez éditer le fichier docker-compose.yml pour définir le mot de passe root de la bdd

#### Installation de la base de données avec le script 

Aller sur l'interface de Planeta: 
`http://localhost:8080/CanteenCalandreta/Admin/Install/index.php`

Cliquer sur suivant pour l'installation de planeta
Entrer `db` pour le Serveur de base de données
Entrer `root` pour l'utilisateur
Entrer `calandreta_planeta` pour le mot de passe
Cliquer ensuite sur suivant jusqu'au bout

#### Bugs version dev3.9
Un bug fait que la base de données n'est pas configurée correctement avec Admin/Install/index.php
Pour configurer la base de données manuellement: 
se connecter à l'interface phpmyadmin sur 
`localhost:8080`
Entrer 
`login: root`
`Mot de passe: calandreta_planeta`
Cliquer sur Planeta à gauche
Cliquer sur Importer en haut
Cliquer sur Parcourir dans Fichier à importer puis choisir le fichier dans le sous repertoire planeta/Admin/Sql/CreateAllTablesDBwithData.sql
Puis jeu de caracteres Iso-8859-1
Puis cliquer en bas sur Importer
Cliquer ensuite sur Planeta à gauche
Cliquer sur la table SupportMembers
Les Colonnes SupportMemberFirstName et SupportMemberLastName correspondent aux login et mot de passes que vous pouvez utiliser au login du logiciel planeta
Remplacer les accents aigus (sinon bug au login) dans les tables pour pouvoir ensuite vous logguer sur 
`http://localhost:8080/CanteenCalandreta/Admin/Install/index.php`

## Réinstaller/remettre à zero planeta

#### Stopper les containers depuis le repertoire où est situé docker-compose.yml 
`docker-compose stop`
`docker rm -f db`
`docker rm -f phpmyadmin`

#### Supprimer les volumes:
`docker volume rm planeta_mydatavolume`

#### Pour relancer planeta depuis le répertoire où est situé docker-compose.ymls: 
`docker-compose up`

## Entrer dans un container pour voir le contenu de certains fichiers

#### Entrer dans le container phpmyadmin (contenant les repertoires de CanteenCalandreta)
`docker exec -it mysql sh`

#### Entrer dans le container db (contenant la base de données)
`docker exec -it db sh`

